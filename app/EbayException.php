<?php namespace Tada;

use Illuminate\Database\Eloquent\Model;

class EbayException extends Model {

//    public function exception()
//    {
//        return $this->belongsTo('Tada\Ebay', 'product_id', 'product_id');
//    }

    public static function on_product_id($id){
        $exceptions = EbayException::where('product_id',$id)->get();
        $exception = (object) array('product_id'=>$id,'additionalname'=>null,'name'=>null,'category'=>null );
        foreach( $exceptions as $ex){
            if($ex->additionalname != null){
                $exception->additionalname .= $ex->additionalname.',';
            }
            if($ex->name != null){
                $exception->name .= $ex->name.',';
            }
            if($ex->category != null){
                $exception->category .= $ex->category.',';
            }
        }
        return $exception;
    }
    
    public static function on_product_id_save($id,$response){
        EbayException::where('product_id',$id)->delete();
        
        $additionalname =explode( ',',$response['additionalname']);
        foreach($additionalname as $addname){
            if(strlen($addname)>1){
                $ebayexcep = new EbayException();
                $ebayexcep->product_id =$id;
                $ebayexcep->additionalname = $addname;
                $ebayexcep->save();
            }
        }
        $names =explode( ',',$response['name']);
        foreach($names as $name){
            if(strlen($name)>1){
                $ebayexcep = new EbayException();
                $ebayexcep->product_id =$id;
                $ebayexcep->name = $name;
                $ebayexcep->save();
            }
        }
        $categorys =explode( ',',$response['category']);
        foreach($categorys as $category){
            if(strlen($category)>1){
                $ebayexcep = new EbayException();
                $ebayexcep->product_id =$id;
                $ebayexcep->category = $category;
                $ebayexcep->save();
            }
        }
        return true;
    }
    
    
}
