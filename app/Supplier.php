<?php namespace Tada;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model {

	protected $fillable =[
        'name',
        'adress',
        'per_item_fee',
        'monthly_fee',
        'active'
        ];

}
