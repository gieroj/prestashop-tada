<?php namespace Tada\Http\Controllers;

use Tada\Http\Requests;
use Tada\Http\Controllers\Controller;

use Request;
use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Finding\Services;
use \DTS\eBaySDK\Finding\Types;

use Tada\Img;
use Tada\Ebay;
use Tada\Products;

class EbayController extends Controller {

        public $paginate_site=9;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            $ebays = Products::where('supplier_id',3)->orderBy('id')->paginate($this->paginate_site);
            $img = array();
            foreach ($ebays as $ebay){
                $img[] = Img::where('product_id',$ebay->id)->first();
            }
//            dd($img);
            return view('pages.ebay',compact('ebays','img'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
            
//		 // Instantiate a service.
//            $service = new Services\FindingService(array(
//                'appId' => 'gieroj9f8-de83-4644-93e6-bb498e00d95',
//                'globalId' => Constants\GlobalIds::US
//            ));
//            
//            // Create the API request object.
//            $request = new Types\FindItemsByKeywordsRequest();
//            
//            // Assign the keywords.
//            $request->keywords = 'Harry Potter';
//
//            // Ask for the first 25 items.
//            $request->paginationInput = new Types\PaginationInput();
//            $request->paginationInput->entriesPerPage = 3;
//            $request->paginationInput->pageNumber = 1;
//
//            // Ask for the results to be sorted from high to low price.
//            $request->sortOrder = 'CurrentPriceHighest';
//            
//            // Send the request.
//            $response = $service->findItemsByKeywords($request);
//            
//          // print_r($response); 
//            // Output the response from the API.
//            if ($response->ack !== 'Success') {
//                foreach ($response->errorMessage->error as $error) {
//                    printf("Error: %s\n", $error->message);
//                }
//            } else {
//                foreach ($response->searchResult->item as $item) {
//                    printf("(%s) %s:%.2f\n",$item->galleryURL,$item->viewItemURL, $item->itemId, $item->title, $item->sellingStatus->currentPrice->value);
//                }
//            }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
//		$ebay = Ebay::where('id',$id)->first();
//                $request = Request::all();
//                $ebay->name = $request['name'];
//                $ebay->price = $request['price'];
//                $ebay->url =$request['url'];
//                $ebay->shipping_fee = $request['shipping_fee'];
//                $ebay->save();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
        
        

}
