<?php namespace Tada\Http\Controllers\Console;

use Tada\Http\Requests;
use Tada\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Tada\Img;
use Tada\Category;
use DB;

class ImporterController extends Controller {

    /**
    * login_and_download
    *
    * login and then download file
    *
    * @url_login (string) url to login on
    * @postinfo (string) example: "login=".$username."&password=".$password
    * @url (string) url to capture
    * @otput (string) path to output file 
    */
    public static function login_and_download($url_login,$postinfo,$url,$output ){
          //set the directory for the cookie using defined document root var
            $dir = "/ctemp";
            //build a unique path with every request to store 
            //the info per user with custom func. 
            $path = $dir;

            $cookie_file_path = $path."/cookie.txt";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_NOBODY, false);
            curl_setopt($ch, CURLOPT_URL, $url_login);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
            //set the cookie the site has for certain features, this is optional
            curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
            curl_setopt($ch, CURLOPT_USERAGENT,
                "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            //curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postinfo);
            curl_exec($ch);

            //page with the content I want to grab
            curl_setopt($ch, CURLOPT_URL, $url);
            //do stuff with the info with DomDocument() etc
            $html = curl_exec($ch);
            curl_close($ch);
            return(file_put_contents($output, $html));
    }
    
    /**
    * get_url_contents
    *
    * get website 
    *
    * @url (string) url to capture
    */
    public static function get_url_contents($url){
        $crl = curl_init();
        $timeout = 5;
        curl_setopt ($crl, CURLOPT_URL,$url);
        curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
        $ret = curl_exec($crl);
        curl_close($crl);
        return $ret;
    }
    
    public static function znajdzAtrybut($object, $attribute) {
        foreach ($object->attributes() as $a => $b) {
            if ($a == $attribute) {
                $return = $b;
            }
        }
        if ($return) {
            return $return;
        }
    }
    
    /**
    * add_img
    *
    * add img to database 
    *
    * @url (string) url to img capture
    * @path (string) path to save img
    * @product_id (int) id of product to img belong 
    */
    public static function add_img($url,$path,$product_id){
        if(strlen($url)>10 && $path!= null && $product_id!= null){
            $img=Img::where('product_id',$product_id)->where('img',substr(strrchr($url, "/"),1))->first();
            if($img){
                //dopisac tu sprawdzanie czy sa zdjęcia w folderze  albo usuwanie i wstawianie nowych zdjec
                return false;
            }else {
                $img = new Img();
                $img->product_id = $product_id;
                $img->location = $path.$product_id;
                $img->img_url = $url;
                $img->img = substr(strrchr($url, "/"),1);
                $img->save();
                ini_set('max_execution_time', 100);
                if(is_dir($img->location)==false){
                    mkdir($img->location,0755);
                    file_put_contents($img->location.'/'.$img->img, file_get_contents($url));
                }else if(is_dir($img->location.'/'.$img->img)==false) {
                //file_put_contents($img->location.'/'.$img->img, fopen($url, 'r'));
                file_put_contents($img->location.'/'.$img->img, file_get_contents($url));
                }
            }
        }
    }
    /**
    * add_affiliate_category(
    *
    * connecting category with products
    *
    * @product_id (int) id of product to conecte
    * @category_id (int) category id if you dont know you can put here 0 or false
    * @category_name (string) send name of category if you dont know his id
    * @supplier_id (int) its also requaied if you dont know category id
    */
    public static function add_affiliate_category($product_id,$category_id,$category_name=Null,$supplier_id=Null){
        if(!$category_name && $category_id>0){
            if(!DB::table('affiliate_category')->where('product_id',$product_id)->where('category_id',$category_id)->first()){  
                DB::insert('insert into affiliate_category (product_id, category_id) values ('.$product_id.','.$category_id.')');
            }
        }else if($category_name && $supplier_id) {
            $category=Category::where('supplier_id','=',$supplier_id)->where('name','=',$category_name)->first();
            if(count($category)>0 && $category->id > 0){
                if(!DB::table('affiliate_category')->where('product_id',$product_id)->where('category_id',$category->id)->first()){ 
                    DB::insert('insert into affiliate_category (product_id, category_id) values ('.$product_id.','.$category->id.')');
                }
            }else{
                echo 'missed category name: '.$category_name.PHP_EOL;
            }
        }else {
            echo 'You miss $category_name or $supplier_id'.PHP_EOL;
        }
    }
}
