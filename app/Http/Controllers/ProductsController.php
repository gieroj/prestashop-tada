<?php namespace Tada\Http\Controllers;

use Tada\Http\Requests;
use Tada\Http\Controllers\Controller;


use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Finding\Services;
use \DTS\eBaySDK\Finding\Types;

use Tada\Products;
use Tada\Ebay;
use Tada\EbayException;
use Tada\Img;
use \DTS\eBaySDK\Shopping\Services as EbaySServices;
use \DTS\eBaySDK\Shopping\Types as EbaySTypes;
use Request;
class ProductsController extends Controller {

        public function __construct() {
            $this->middleware('auth');
        }
        public $paginate_site=9;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = Products::leftJoin('imgs', 'products.id', '=', 'imgs.product_id')->
                        where('products.status',1)->orderBy('products.id')->groupBy('products.id')->paginate($this->paginate_site);
                return view('pages.products',compact('products'));
	}
        
        /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
        public function sortBy($name=null)
	{
            if($name){
                if($name == 'profit'){
                    $products = Products::leftJoin('imgs', 'products.id', '=', 'imgs.product_id')->where('products.status',1)->
                            orderByRaw("products.msrp_price -  products.supplier_price")->groupBy('products.id')->paginate($this->paginate_site);
                    //$products = Products::select("(msrp_price - supplier_price) as profit")->orderBy('profit')->paginate($this->paginate_site);
                }else if($name == 'ebay_profit'){
                    $products = Products::leftJoin('imgs', 'products.id', '=', 'imgs.product_id')->where('products.status',1)->
                            orderByRaw("products.ebay_price -  products.supplier_price")->groupBy('products.id')->paginate($this->paginate_site);
                }else
                {
                    $products = Products::leftJoin('imgs', 'products.id', '=', 'imgs.product_id')->where('products.status',1)->
                            orderBy('products.'.$name)->groupBy('products.id')->paginate($this->paginate_site);
                }
            }else{
                $products = Products::leftJoin('imgs', 'products.id', '=', 'imgs.product_id')->where('products.status',1)->
                        orderBy('products.id')->groupBy('products.id')->paginate($this->paginate_site);
            }
           // return dd($products);
            return view('pages.products',compact('products'));
        }
        
        public function dsortBy($name=null)
	{
            if($name){
                if($name == 'profit'){
                    $products = Products::leftJoin('imgs', 'products.id', '=', 'imgs.product_id')->where('products.status',1)->
                            orderByRaw("products.msrp_price -  products.supplier_price desc")->groupBy('products.id')->paginate($this->paginate_site);
                }else if($name == 'ebay_profit'){
                    $products = Products::leftJoin('imgs', 'products.id', '=', 'imgs.product_id')->where('products.status',1)->
                            orderByRaw("products.ebay_price -  products.supplier_price desc")->groupBy('products.id')->paginate($this->paginate_site);
                }else{
                    $products = Products::leftJoin('imgs', 'products.id', '=', 'imgs.product_id')->where('products.status',1)->
                            orderBy('products.'.$name, 'desc')->groupBy('products.id')->paginate($this->paginate_site);
                }
            }else{
                $products = Products::leftJoin('imgs', 'products.id', '=', 'imgs.product_id')->where('products.status',1)->
                        orderBy('products.id', 'desc')->groupBy('products.id')->paginate($this->paginate_site);
            }
           // return dd($products);
            return view('pages.products',compact('products'));
        }
        
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\ProductsRequest $request)
	{
            $input = $request->all();
		dd( $input); 
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
            $product = Products::FindOrFail($id);
            return view('pages.product',compact('product'));
            
//            $endpoint = "https://api.ebay.com/ws/api.dll";
// 
//            $api_dev_name = "165a687d-a6ab-4461-94d6-92ee2d4616cc";
//            $api_app_name = "gieroj9f8-de83-4644-93e6-bb498e00d95";
//            $api_cert_name = "a52e0f31-39f2-429c-a562-4fdc34a1cbe4";
//            $auth_token = "AgAAAA**AQAAAA**aAAAAA**F3o4VQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wHl"
//                    . "oapAZOCpwudj6x9nY+seQ**Mr8BAA**AAMAAA**7k1COMZn0TIhh7vTCVLK57f/cMrXSf"
//                    . "O3wqo7yZaBaO6IH9VJ6zl1qLvylR+sR0F04nAbOU+405X7n8UIiPmmTFJD5HnoUCxknrl"
//                    . "LH/UbHDm+ltRywMcieu5pRwZDTpoKSFec8TRbjpwcBArV4TyEUKjw+JDuUYPkOB2uu/f+"
//                    . "JfLjz0IU0MQ6rwWOTk/Yve4UkgJ/gNnk9zxdrtmcaveb8LW5chmsAQpF1DW43njhmQXMJ"
//                    . "9SHT5Y/QFCNOvx1+bnRhzdG+1ZBbGg8ey2AaMR2my+EI7Bk3gvvQ6Q4Xkb8UGvTcMw2xwe"
//                    . "8qjO1+MgvOIcoq6N0Gk4alsV6xvifth+3XtzxzOMCV4Be8+K+BH7TKagMjBPKRbKbUJIKM"
//                    . "wlhQgEWjMzB/AoxBlKVmI1clhumMtZXeA7lMH+hsPXmsalNzny1kqkfy+0oakAmLR2n3uz"
//                    . "V2iufH4MjZ1EwHw8SdxPgFEBurKg3r0DgQd87mdqbJ7A58RGnpDsFFBVPIVpFEdgdF/eC"
//                    . "na/u1ApJAWpZZ+sgGYLEA7XE/TWfuPqziTpgC6UG+CtpDSvt8J8CHLxqy4Tyg8fYmXUmD"
//                    . "BQMs8c+UtmPeYrUtUfnNax6IM68cqS1bXeSBofmqkgF5vY8R/OqECslM7M7kPYYvlIE2Z"
//                    . "j2lh3huYCygw6Ul58MrE7FhPPmfkH27rwvJgBdyKUCuRTYNJRiX4nYa/QbPZLURdR67/q"
//                    . "qJRTrznPHDigTXhzOPDW3XZbY8ry6tzJ2L18ZfWpKRbTm";
// 
//                    $headers = array(
//                            'Content-Type:text/xml',
//                            'X-EBAY-API-COMPATIBILITY-LEVEL: 967',
//                            'X-EBAY-API-DEV-NAME: '.$api_dev_name,
//                            'X-EBAY-API-APP-NAME: '.$api_app_name,
//                            'X-EBAY-API-CERT-NAME :'.$api_cert_name,
//                            'X-EBAY-API-CALL-NAME: GetCategories',
//                            'X-EBAY-API-SITEID: 3' # eBay.co.uk
//                    );
//
//$body = <<<BODY
//<?xml version="1.0" encoding="utf-8"
//<GetCategoriesRequest xmlns="urn:ebay:apis:eBLBaseComponents">
//<RequesterCredentials>
//<eBayAuthToken>$auth_token</eBayAuthToken>
//</RequesterCredentials>
//<ViewAllNodes>False</ViewAllNodes>
//<DetailLevel>ReturnAll</DetailLevel>
//</GetCategoriesRequest>
//BODY;
//                    $body = utf8_encode($body);
// 
//                    $curl = curl_init();
//
//                    curl_setopt_array($curl,
//                            array(
//                                    CURLOPT_RETURNTRANSFER => 1,
//                                    CURLOPT_URL => $endpoint,
//                                    CURLOPT_POST => 1,
//                                    CURLOPT_POSTFIELDS => $body,
//                                    CURLOPT_HTTPHEADER => $headers
//                            )
//                    );
//
//                    $response = curl_exec($curl);
//                    curl_close($curl);
//
//                    if(!$response){
//                        die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
//                    }
//                    echo $response;

           

                // return Response::make($response,'200')->header('Content-Type', 'text/xml');//view('pages.product',compact('response'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{   
            $imgs = Img::where('product_id',$id)->get();
            $product = Products::FindOrFail($id);
            $ebay = Ebay::where('product_id',$id)->get();
            $exception = EbayException::on_product_id($id);
            return view('pages.productedit',compact('product','imgs','ebay','exception'));
	}
        
        /**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function exception_update($id)
	{
            
            EbayException::on_product_id_save($id,Request::all());
//            dd(Request::all()) ;
            $this->download_ebay_exception($id);
            return redirect('products/'.$id.'/edit'); 
        }
        
        public function ebay_update($id)
	{
            $request = Request::all();
            $ebay1 = Ebay::where('id',$request['id'])->first();
            
          //  dd($request ,$ebay1);
            $ebay1->name = $request['name'];
            $ebay1->price = $request['price'];
            $ebay1->url =$request['url'];
            $ebay1->shipping_fee = $request['shipping_fee'];
            $ebay1->save();

            Ebay::save_minimal_price($id);
            return redirect('products/'.$id.'/edit');
        }
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
//            $products = Products::FindOrFail($id);
//            $ebay = Ebay::FindOrFail($id);
//            $additionalname = EbayAdditionalName::where('product_id', $id)->FindOrFail(1);
//            dd(Input::all()); 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
        
        public function download_ebay_exception($id_product)
        {
            
//            dd($exception);
            // Instantiate a service.
               $service = new Services\FindingService(array(
                   'appId' => 'gieroj9f8-de83-4644-93e6-bb498e00d95',
                   'globalId' => Constants\GlobalIds::US
               ));
               $shopping_service = new EbaySServices\ShoppingService(array(
                    'apiVersion' => '863',
                    'appId' => 'gieroj9f8-de83-4644-93e6-bb498e00d95'
                ));
               Ebay::where('product_id',$id_product)->delete();// truncate();
               //EbayException::truncate();
            $product = Products::where('supplier_id',2)->where('id',$id_product)->first();

            $ebayex = EbayException::where('product_id',$id_product)->get();

            $exception_additionalname='';
            $exception_name= '';
            $exception_category= array();
            $ask='';
            if (isset($ebayex[0]))
                {
                foreach( $ebayex as $ex){
                    if($ex->additionalname != null){
                        $exception_additionalname .= $ex->additionalname.',';
                    }
                    if($ex->name != null){
                        $exception_name .= $ex->name.',';
                    }
                    if($ex->category != null){
                        $exception_category[] = $ex->category ;
                    }
                }
                if(strlen($exception_additionalname)>2){
                    if(strlen($exception_name)>2){
                        $ask ='('.substr($exception_additionalname,0,-1).') - ('.substr($exception_name,0,-1).')';
                    }else{
                        $ask ='('.substr($exception_additionalname,0,-1).')';
                    }
                }
            }
            echo 'start';
            if (isset($exception_category)){
                for($i=0;$i<count($exception_category);$i++){
                    // Create the API request object.
                    $request = new Types\FindItemsAdvancedRequest();

                    // Assign the keywords.
                   echo $product->id.PHP_EOL;
                    $request->keywords = $ask;
                    $request->categoryId[] = $exception_category[$i];
                    // Ask for the first 25 items.
                    $request->paginationInput = new Types\PaginationInput();
                    $request->paginationInput->entriesPerPage = 10;
                    $request->paginationInput->pageNumber = 1;

                    // Ask for the results to be sorted from high to low price.
                    $request->sortOrder = 'CurrentPriceLowest';
    //                dd($request);
                    // Send the request.
                    $response = $service->findItemsAdvanced($request);

                  // print_r($response); 
                    // Output the response from the API.
                    echo 'midle'.PHP_EOL;
                    if ($response->ack !== 'Success') {
                        foreach ($response->errorMessage->error as $error) {
                            echo("Error: %s\n". $error->message);
                        }
                    } else {
                        foreach ($response->searchResult->item as $item) {

                            $ebay = new Ebay();
                            $ebay->product_id = $product->id;
                            $ebay->name = $item->title;
                            $ebay->img = $item->galleryURL;
                            $ebay->url = $item->viewItemURL;
                            $ebay->price = $item->sellingStatus->currentPrice->value;
                            $ebay->shipping_fee = 0;
                            if(isset($item->shippingInfo->shippingServiceCost->value)){
                                $ebay->shipping_fee = $item->shippingInfo->shippingServiceCost->value;
                            }else if(isset($item->itemId)){
                                $shipp_request = new EbaySTypes\GetShippingCostsRequestType();
                                $shipp_request->DestinationCountryCode = 'US';
                                $shipp_request->DestinationPostalCode = '60634';
                                $shipp_request->IncludeDetails = false;
                                $shipp_request->ItemID = $item->itemId;
    //                            dd($shipp_request);
                                $shipp_response = $shopping_service->GetShippingCosts($shipp_request);
                               // dd($shipp_response);
                                //echo $shipp_response->ShippingCostSummary->ShippingServiceCost->value.PHP_EOL;
                                $ebay->shipping_fee = $shipp_response->ShippingCostSummary->ShippingServiceCost->value;
                            }
                            $ebay->save();
                            //dd("(%s) %s:%.2f\n",$item->galleryURL,$item->viewItemURL, $item->itemId, $item->title, $item->sellingStatus->currentPrice->value);
                        }
                    }
                }
            }

            
            Ebay::save_minimal_price($id_product);
	}
//            {
//            // Instantiate a service.
//            $service = new Services\FindingService(array(
//                'appId' => 'gieroj9f8-de83-4644-93e6-bb498e00d95',
//                'globalId' => Constants\GlobalIds::US
//            ));
//            $ask='';
//            $additionalname = '';
//            $name = '';
//            $category = array();
//            $ebay_exception = EbayException::where('product_id',$id_product)->get();
//            foreach ($ebay_exception as $ex){
//                if(strlen($ex->additionalname)>1){
//                    $additionalname .= $ex->additionalname.',';
//                }
//                if(strlen($ex->name)>1){
//                    $name .= $ex->name.',';
//                }
//                if(strlen($ex->category)>1){
//                    $category = $ex->category;
//                }
//            }
//            if(strlen($additionalname)>2){
//                if(strlen($name)>2){
//                    $ask ='('.substr($additionalname,0,-1).') - ('.substr($name,0,-1).')';
//                }else{
//                    $ask ='('.substr($additionalname,0,-1).')';
//                }
//            }
////            echo $ask;
//            if(strlen($ask)>2){
//                Ebay::where('product_id',$id_product)->delete();
//            // Create the API request object.
//                $request = new Types\FindItemsAdvancedRequest();
//
//                // Assign the keywords.
//                $request->keywords = $ask;
//                $request->categoryId[] = $category;
//                // Ask for the first 25 items.
//                $request->paginationInput = new Types\PaginationInput();
//                $request->paginationInput->entriesPerPage = 20;
//                $request->paginationInput->pageNumber = 1;
//
//                // Ask for the results to be sorted from high to low price.
//                $request->sortOrder = 'CurrentPriceLowest';
//               // dd($request);
//                // Send the request.
//                $response = $service->findItemsAdvanced($request);
//
//              // print_r($response); 
//                // Output the response from the API.
//                if ($response->ack !== 'Success') {
//                    foreach ($response->errorMessage->error as $error) {
//                        dd("Error: %s\n", $error->message);
//                    }
//                } else {
//                    foreach ($response->searchResult->item as $item) {
//                     
//                        $ebay = new Ebay();
//                        $ebay->product_id = $id_product;
//                        $ebay->name = $item->name;
//                        $ebay->img = $item->galleryURL;
//                        $ebay->url = $item->viewItemURL;
//                        $ebay->price = $item->sellingStatus->currentPrice->value;
//                        $ebay->shipping_fee = 0;
//                        if(isset($item->shippingInfo->shippingServiceCost->value)){
//                            $ebay->shipping_fee = $item->shippingInfo->shippingServiceCost->value;
//                        }else {
//                            $ebay->shipping_fee = null;
//                        }
//                        $ebay->save();
//                        //dd("(%s) %s:%.2f\n",$item->galleryURL,$item->viewItemURL, $item->itemId, $item->title, $item->sellingStatus->currentPrice->value);
//                    }
//                }
//            } else {
//                return false;
//            }
//            
//            Ebay::save_minimal_price($id_product);
//        }


}
