<?php namespace Tada\Http\Controllers;

use Tada\Http\Requests;
use Tada\Http\Controllers\Controller;

//use Illuminate\Http\Request;
use Tada\MyEbay;
use Tada\Img;
use Tada\Products;
use Tada\Shipper;
use Request;

use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;

class MyEbayController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($id)
	{
            $imgs = Img::where('product_id',$id)->get();
            $product = Products::FindOrFail($id);
            $myebay= new MyEbay();
            $shippers = Shipper::lists('name');
//            $shipments = array();
//            foreach( $shipmentes as $ship){
//                $shipments[] = $ship->name;
//            }
            
            return view('pages.myebaycreate',compact('product','imgs','myebay','shippers'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
           
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            $myrequest = Request::all();
//            dd($myrequest);
            
            $siteId = Constants\SiteIds::US;
            
            $service = new Services\TradingService(array(
                'apiVersion' => '899',
                'sandbox' => true,
                'siteId' => $siteId
            ));
            
            /**
            * Create the request object.
            *
            * For more information about creating a request object, see:
            * http://devbay.net/sdk/guides/getting-started/#request-object
            */
           $request = new Types\AddItemRequestType();
           /**
            * An user token is required when using the Trading service.
            *
            * For more information about getting your user tokens, see:
            * http://devbay.net/sdk/guides/application-keys/
            */
           $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
           $request->RequesterCredentials->eBayAuthToken =  env('EBAY_SANDBOX_TOKEN') ;
           /**
            * Begin creating the auction item.
            */
           $item = new Types\ItemType();
           /**
            * We want a single quantity auction.
            * Otherwise known as a Chinese auction.
            */
           $item->ListingType = Enums\ListingTypeCodeType::C_FIXED_PRICE_ITEM;
           $item->Quantity = (int)$myrequest['quantity'];
           /**
            * Let the auction run for 7 days.
            */
           $item->ListingDuration = Enums\ListingDurationCodeType::C_DAYS_7;
           $item->StartPrice = new Types\AmountType(array('value' => (double)$myrequest['my_price']));
//           $item->StartPrice = new Types\AmountType(array('value' => 8.99));
//            /**
//             * State that the item will not sell if none of the bids meet the reserve price of $10.99
//             */
//            $item->ReservePrice = new Types\AmountType(array('value' => 10.99));
           /**
            * Let buyers end the auction immediately by purchasing the item at the Buy It Now price of $15.99
            * This also shows how we can specify the currency if we wanted to.
            */
//           $item->BuyItNowPrice = new Types\AmountType(array('value' => 55.77, 'currencyID' => 'USD'));
           /**
            * Provide a title and description and other information such as the item's location.
            * Note that any HTML in the title or description must be converted to HTML entities.
            */
           $item->Title = $myrequest['name'];
           $item->Description = '<![CDATA['.$myrequest['description'].']]>'; //strip_tags($myrequest['description']);
           $item->Country = 'US';
           $item->Location = 'El Monte';
           $item->PostalCode = '91731';
           /**
            * This is a required field.
            */
           $item->Currency = 'USD';
           /**
            * Display a picture with the item.
            */
           $imgs = Img::where('product_id',$id)->get();
           $img_array= array();
           
           foreach ($imgs as $img){
                $img_array[]=$img['img_url'];
           }
           
           $item->PictureDetails = new Types\PictureDetailsType();
           $item->PictureDetails->GalleryType = Enums\GalleryTypeCodeType::C_GALLERY;
           $item->PictureDetails->PictureURL = $img_array;// array('http://lorempixel.com/1500/1024/abstract');
           /**
            * List item in the Books > Audiobooks (29792) category.
            */
           $item->PrimaryCategory = new Types\CategoryType();
           $item->PrimaryCategory->CategoryID = '29792';
           /**
            * Tell buyers what condition the item is in.
            * For the category that we are listing in the value of 1000 is for Brand New.
            */
           $item->ConditionID = 1000;
           /**
            * Buyers can use one of two payment methods when purchasing the item.
            * Visa / Master Card
            * PayPal
            * The item will be dispatched within 3 business days once payment has cleared.
            * Note that you have to provide the PayPal account that the seller will use.
            * This is because a seller may have more than one PayPal account.
            */
           $item->PaymentMethods = array(
               'VisaMC',
               'PayPal'
           );
           $item->PayPalEmailAddress = 'example@example.com';
           $item->DispatchTimeMax = 3;
           /**
            * Setting up the shipping details.
            * We will use a Flat shipping rate for both domestic and international.
            */
           $item->ShippingDetails = new Types\ShippingDetailsType();
           $item->ShippingDetails->ShippingType = Enums\ShippingTypeCodeType::C_FLAT;
           /**
            * Create our first domestic shipping option.
            * Offer the Economy Shipping (1-10 business days) service for $2.00.
            */
           $shippingService = new Types\ShippingServiceOptionsType();
           $shippingService->ShippingServicePriority = 1;
           $shippingService->ShippingService = 'Other';
           echo (double) $myrequest['description'];
           $shippingService->ShippingServiceCost = new Types\AmountType(array('value' =>(double) $myrequest['description']));
           $item->ShippingDetails->ShippingServiceOptions[] = $shippingService;
           /**
            * Create our second domestic shipping option.
            * Offer the USPS Parcel Select (2-9 business days) for $3.00.
            */
//           $shippingService = new Types\ShippingServiceOptionsType();
//           $shippingService->ShippingServicePriority = 2;
//           $shippingService->ShippingService = 'USPSParcel';
//           $shippingService->ShippingServiceCost = new Types\AmountType(array('value' => 3.00));
//           $item->ShippingDetails->ShippingServiceOptions[] = $shippingService;
           /**
            * Create our first international shipping option.
            * Offer the USPS First Class Mail International service for $4.00.
            * The item can be shipped Worldwide with this service.
            */
//           $shippingService = new Types\InternationalShippingServiceOptionsType();
//           $shippingService->ShippingServicePriority = 1;
//           $shippingService->ShippingService = 'USPSFirstClassMailInternational';
//           $shippingService->ShippingServiceCost = new Types\AmountType(array('value' => 4.00));
//           $shippingService->ShipToLocation = array('WorldWide');
//           $item->ShippingDetails->InternationalShippingServiceOption[] = $shippingService;
           
           /**
            * Create our second international shipping option.
            * Offer the USPS Priority Mail International (6-10 business days) service for $5.00.
            * The item will only be shipped to the following locations with this service.
            * N. and S. America
            * Canada
            * Australia
            * Europe
            * Japan
            */
//           $shippingService = new Types\InternationalShippingServiceOptionsType();
//           $shippingService->ShippingServicePriority = 2;
//           $shippingService->ShippingService = 'USPSPriorityMailInternational';
//           $shippingService->ShippingServiceCost = new Types\AmountType(array('value' => 5.00));
//           $shippingService->ShipToLocation = array(
//               'Americas',
//               'CA',
//               'AU',
//               'Europe',
//               'JP'
//           );
           $item->ShippingDetails->InternationalShippingServiceOption[] = $shippingService;

           
           /**
            * The return policy.
            * Returns are accepted.
            * A refund will be given as money back.
            * The buyer will have 14 days in which to contact the seller after receiving the item.
            * The buyer will pay the return shipping cost.
            */
           $item->ReturnPolicy = new Types\ReturnPolicyType();
           $item->ReturnPolicy->ReturnsAcceptedOption = 'ReturnsAccepted';
           $item->ReturnPolicy->RefundOption = 'MoneyBack';
           $item->ReturnPolicy->ReturnsWithinOption = 'Days_14';
           $item->ReturnPolicy->ShippingCostPaidByOption = 'Buyer';
           /**
            * Finish the request object.
            */
           
           $request->Item = $item;
           /**
            * Send the request to the AddItem service operation.
            *
            * For more information about calling a service operation, see:
            * http://devbay.net/sdk/guides/getting-started/#service-operation
            */
           $response = $service->addItem($request);
           /**
            * Output the result of calling the service operation.
            *
            * For more information about working with the service response object, see:
            * http://devbay.net/sdk/guides/getting-started/#response-object
            */
           if (isset($response->Errors)) {
               foreach ($response->Errors as $error) {
                   printf("%s: %s\n%s\n\n",
                       $error->SeverityCode === Enums\SeverityCodeType::C_ERROR ? 'Error' : 'Warning',
                       $error->ShortMessage,
                       $error->LongMessage
                   );
               }
           }
           if ($response->Ack !== 'Failure') {
               printf("The item was listed to the eBay Sandbox with the Item number %s\n",
                   $response->ItemID
               );
           }

            
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
