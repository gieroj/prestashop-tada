<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('products/sortBy/{name?}', 'ProductsController@sortBy');
Route::get('products/dsortBy/{name?}', 'ProductsController@dsortBy');
Route::patch('products/{id}/exception', 'ProductsController@exception_update');
Route::patch('products/{id}/ebayupdate', 'ProductsController@ebay_update');
Route::resource('products', 'ProductsController');

Route::get('myebay/create/{id}', 'MyEbayController@create');
Route::resource('myebay', 'MyEbayController');

Route::resource('suppliers', 'SuppliersController');
//Route::resource('ebay', 'EbayController');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
