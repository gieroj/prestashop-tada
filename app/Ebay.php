<?php namespace Tada;

use Illuminate\Database\Eloquent\Model;
use Tada\Products;
class Ebay extends Model {
    
//    public function exception()
//    {
//        return $this->hasMany('Tada\EbayException');
//    }
    public static function save_minimal_price($id_product){
        $product = Products::where('id',$id_product)->first();
        $ebays = Ebay::where('product_id',$id_product)->get();
        $mini=0;
        foreach ($ebays as $ebay){
            $cost = $ebay->price + $ebay->shipping_fee;
            if ($mini==0 || $mini>$cost){
                $mini=$cost;
            }
        }
        $product->ebay_price = $mini;
        $product->save();
    }

}
