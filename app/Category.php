<?php namespace Tada;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model {

	protected $fillable =[
        'supplier_id',
        'name',
        'parent_id'
        ];
        
        public static function product_id($product_id){
           return( DB::table('affiliate_category')->leftJoin('categories','affiliate_category.category_id','=','categories.id')
                    ->where('affiliate_category.product_id',$product_id)->get()); 
        }

}
