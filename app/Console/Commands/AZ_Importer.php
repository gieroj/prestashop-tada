<?php namespace Tada\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Tada\Http\Controllers\Console\ImporterController;
use Tada\Products;


use Excel;
//use DB;

class AZ_Importer extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'import:azimport';
        private $img_folder = 'public/importy/dane/AZImporter/';
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import AZ Importer csv in to database.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
            //username and password of account
            $csv_output= 'public/importy/AZ_Importer2.csv';
//            $username = trim(env('INVENTORY_LOGIN'));
//            $password = trim(env('INVENTORY_PASSWORD'));
//            $csv_output= 'importy/AZ_Importer.csv';
//
//            //login form action url
//            $url_login="https://www.inventorysource.com/InventorySourcev2/admin/DoLogin"; 
//            $postinfo = "login=".$username."&password=".$password;
//            $url ="https://www.inventorysource.com/InventorySourcev2/admin/FileDownload.dnld?fileType=active&fileFormat=3&dropshipper=155&file=1&fname=3AZImporterACTIVE.csv";
//            echo(ImporterController::login_and_download($url_login,$postinfo,$url,$csv_output).PHP_EOL);
   
            
            //import from manualy put csv
            //Send csv in to database
            $result = Excel::load($csv_output, 'UTF-8');
            
            //Set statusa as false then update product in efect left only product from csv 
            Products::where('supplier_id','=',2)->update(['status' => false]);
            $result->each(function($row) {
                $product = Products::where('sku','=','AI'.$row->item_sku)->where('supplier_id','=',2)->first();
                if ($product){
                    $product->supplier_id = 2;
                    $product->sku = 'AI'.$row->item_sku;
                    $product->name = $row->item_name;
                    $product->short_description = $row->short_description;
                    $product->msrp_price =(float) $row->msrp;
                    $product->permalink = $row->short;
                    $product->status = false; 
                    $product->weight = $row->weight_per_unit;
                    $product->quantity =(int)0;
                    $product->supplier_price =(float)0.00;
                    
                    $product_website=ImporterController::get_url_contents('http://azimporter.com/index.php/'.$row->short.'.html');
                    $start_description=strpos ($product_website,'<table class="data-table" id="product-attribute-specs-table">');//'<div class="product-collateral">');
                    if($start_description){
                        $web_description= substr($product_website,$start_description);
                        $web_description2 = substr($web_description,0,strpos ($web_description, '</table>'));
                        $web_description2.='</table>';
                        if(strpos($web_description2, '://www.youtube.com')){
                            $youtube_link = 'https';
                            $youtube_link .= substr($web_description2,strpos ($web_description2, '://www.youtube.com'));
                            $youtube_link = substr($youtube_link,0,strpos ($youtube_link, '&'));

                            $web_description_start = substr($web_description2,0,strpos ($web_description2, '<object'));
                            $web_description_footer = substr($web_description2,strpos ($web_description2, '</object>')+10);
                            $web_description2 = $web_description_start.'<iframe width="540" height="370" src="'.$youtube_link.
                                    '" frameborder="0" allowfullscreen></iframe>'.$web_description_footer;
                             //dd( $web_description_footer);
                        }
                        echo $row->item_sku.PHP_EOL;
                        $product->description = $web_description2;
                        if(strlen($web_description2)<10){
                            echo $web_description.PHP_EOL.'\033[31m end \033[0m'.PHP_EOL;
                        }
                    }
                    
                    // dd($product);
                    $product->save();
                    
                    $pieces = explode(',', $row->category);
                    foreach($pieces as $pice){
                            ImporterController::add_affiliate_category($product->id, 0, $pice, 2);
                    }
                } else if($row->item_name) {
                    $product = new Products();
                    $product->supplier_id = 2;
                    $product->sku = 'AI'.$row->item_sku;
                    $product->name = $row->item_name;
                    $product->short_description = $row->short_description;
                    $product->msrp_price = (float) $row->msrp;
                    $product->permalink = $row->short;
                    $product->status = false; 
                    $product->weight = $row->weight_per_unit;
                    $product->quantity =(int)0;
                    $product->supplier_price =(float)0.00;
                   
                    
                    $product_website=ImporterController::get_url_contents('http://azimporter.com/index.php/'.$row->short.'.html');
                    $start_description=strpos ($product_website, '<table class="data-table" id="product-attribute-specs-table">');//'<div class="product-collateral">');
                    if($start_description){
                        $web_description= substr($product_website,$start_description);
                        $web_description2 = substr($web_description,0,strpos ($web_description, '</table>'));
                        $web_description2.='</table>';
                        if(strpos($web_description2, '://www.youtube.com')){
                            $youtube_link = 'https';
                            $youtube_link .= substr($web_description2,strpos ($web_description2, '://www.youtube.com'));
                            $youtube_link = substr($youtube_link,0,strpos ($youtube_link, '&'));

                            $web_description_start = substr($web_description2,0,strpos ($web_description2, '<object'));
                            $web_description_footer = substr($web_description2,strpos ($web_description2, '</object>')+10);
                            $web_description2 = $web_description_start.'<iframe width="540" height="370" src="'.$youtube_link.
                                    '" frameborder="0" allowfullscreen></iframe>'.$web_description_footer;
                             //dd( $web_description_footer);
                        }
                        $product->description = $web_description2;
                    }
                    
                    // dd($product);
                    $product->save();
                    
                    
                    ImporterController::add_img($row->images1,$this->img_folder,$product->id);
                    ImporterController::add_img($row->images2,$this->img_folder,$product->id);
                    ImporterController::add_img($row->images3,$this->img_folder,$product->id);
                    ImporterController::add_img($row->images4,$this->img_folder,$product->id);
                    ImporterController::add_img($row->images5,$this->img_folder,$product->id);
                    ImporterController::add_img($row->images6,$this->img_folder,$product->id);
                    ImporterController::add_img($row->images7,$this->img_folder,$product->id);
                    ImporterController::add_img($row->images8,$this->img_folder,$product->id);
                    
                    $pieces = explode(',', $row->category);
                    foreach($pieces as $pice){
                            ImporterController::add_affiliate_category($product->id, 0, $pice, 2);
                    }
                }

                
            });
            
            //get quqntity and price
            $website = ImporterController::get_url_contents("http://aziportal.com/Filter_Data/inv2.html");
            $pieces = explode('<tr valign="TOP">', $website);
            foreach($pieces as $tr){
                $line = explode('</td>', $tr);
                $sku_quantity_price = array();
                foreach($line as $key=>$li){
                    $data=substr(strrchr($li, ">"), 1);
                    if($key == 0){ 
                        $sku_quantity_price[0] = 'AI'.substr($data,0,-1);
                    }else if($key == 1 ){ 
                       $sku_quantity_price[1] = $data; 
                    }else if($key == 2 ){ 
                        $sku_quantity_price[2] = $data;
                    }
                }
                
                $product = Products::where('sku','=',$sku_quantity_price[0])->where('supplier_id','=',2)->first();
                if($product){
                    $product->quantity =(int) $sku_quantity_price[1];
                    $product->supplier_price =(float) $sku_quantity_price[2] + 1.5;
                    if($sku_quantity_price[1]>0){
                        $product->status = true; 
                    }
                    $product->save();
                }else if(count($sku_quantity_price)>2){
//                    Mail::raw((string)$sku_quantity_price[0], function($message)
//                    {
//                        $message->from('info@tada.center', 'Laravel');
//                        $message->to('gieroj@gmail.com', 'John Smith')->subject( "oki");
//                    });
                    echo $sku_quantity_price[0].' '.$sku_quantity_price[1].' '. $sku_quantity_price[2].PHP_EOL;
                } 
            } 
           
	}
        
        

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
