<?php namespace Tada\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


use Tada\Ebay;
use Tada\Category;
use Tada\EbayException;
use Tada\Products;
use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Finding\Services;
use \DTS\eBaySDK\Finding\Types;
use \DTS\eBaySDK\Shopping\Services as EbaySServices;
use \DTS\eBaySDK\Shopping\Types as EbaySTypes;
use DB;


class eBayImporter extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'import:ebay';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Download price from eBay';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
            
//            dd($exception);
            // Instantiate a service.
               $service = new Services\FindingService(array(
                   'appId' => 'gieroj9f8-de83-4644-93e6-bb498e00d95',
                   'globalId' => Constants\GlobalIds::US
               ));
               $shopping_service = new EbaySServices\ShoppingService(array(
                    'apiVersion' => '863',
                    'appId' => 'gieroj9f8-de83-4644-93e6-bb498e00d95'
                ));
               Ebay::truncate();
               //EbayException::truncate();
            $products = Products::where('supplier_id',2)->get();
            foreach($products as $product){
                $ebayex = EbayException::where('product_id',$product->id)->get();

                $exception_additionalname='';
                $exception_name= '';
                $exception_category= array();
                $ask='';
                if (!isset($ebayex[0])){
                    $ebayex = new EbayException();
                    $sku = substr($product->sku,2);
                    if(strpos($sku, ' ')){
                        $sku = substr($sku,0 ,strpos($sku, ' '));
                    }
                   // echo $sku.'   '.$product->sku.PHP_EOL;
                    $ebayex->product_id = $product->id;
                    $ebayex->additionalname = $sku;
                    $ask = $sku;
                    $ebayex->save();
                    $categories = Category::product_id($product->id);
//                    dd( $categories);
                    $zz=0;
                    foreach ($categories as $cat){
                        $egzist = EbayException::where('product_id',$product->id)->where('category',$cat->ebay)->first();
                        if(!isset($egzist)){
                            $ebayex2 = new EbayException();
                            $ebayex2->product_id = $product->id;
                            $ebayex2->category = $cat->ebay;
                            if($zz==0){
                                $exception_category[] = $cat->ebay;
                                $zz++;
                            }else {
                                $exception_category[] = $cat->ebay;
                            }
                            $ebayex2->save();
                        }
                    }
                }else{
                    foreach( $ebayex as $ex){
                        if($ex->additionalname != null){
                            $exception_additionalname .= $ex->additionalname.',';
                        }
                        if($ex->name != null){
                            $exception_name .= $ex->name.',';
                        }
                        if($ex->category != null){
                            $exception_category[] = $ex->category ;
                        }
                    }
                    if(strlen($exception_additionalname)>2){
                        if(strlen($exception_name)>2){
                            $ask ='('.substr($exception_additionalname,0,-1).') - ('.substr($exception_name,0,-1).')';
                        }else{
                            $ask ='('.substr($exception_additionalname,0,-1).')';
                        }
                    }
                }
                echo 'start';
                if (isset($exception_category)){
                    for($i=0;$i<count($exception_category);$i++){
                        // Create the API request object.
                        $request = new Types\FindItemsAdvancedRequest();
                        
                        // Assign the keywords.
                       echo $product->id.PHP_EOL;
                        $request->keywords = $ask;
                        $request->categoryId[] = $exception_category[$i];
                        // Ask for the first 25 items.
                        $request->paginationInput = new Types\PaginationInput();
                        $request->paginationInput->entriesPerPage = 10;
                        $request->paginationInput->pageNumber = 1;

                        // Ask for the results to be sorted from high to low price.
                        $request->sortOrder = 'CurrentPriceLowest';
                        // Send the request.
//                        var_dump($request);
                        $response = $service->findItemsAdvanced($request);

                      // print_r($response); 
                        // Output the response from the API.
                        echo 'midle'.PHP_EOL;
                        if ($response->ack !== 'Success') {
                            foreach ($response->errorMessage->error as $error) {
                                echo("Error: %s\n". $error->message);
                            }
                        } else {
//                            globalId  EBAY-GB
                            foreach ($response->searchResult->item as $item) {
//                                var_dump($item);
                                if($item->globalId == 'EBAY-US'){
                                    $ebay = new Ebay();
                                    $ebay->product_id = $product->id;
                                    $ebay->name = $item->title;
                                    $ebay->img = $item->galleryURL;
                                    $ebay->url = $item->viewItemURL;
                                    $ebay->price = $item->sellingStatus->currentPrice->value;
                                    $ebay->shipping_fee = 0;
                                    if(isset($item->shippingInfo->shippingServiceCost->value)){
                                        $ebay->shipping_fee = $item->shippingInfo->shippingServiceCost->value;
                                    }else if(isset($item->itemId)){
                                        $shipp_request = new EbaySTypes\GetShippingCostsRequestType();
                                        $shipp_request->DestinationCountryCode = 'US';
                                        $shipp_request->DestinationPostalCode = '60634';
                                        $shipp_request->IncludeDetails = false;
                                        $shipp_request->ItemID = $item->itemId;
            //                            dd($shipp_request);
                                        $shipp_response = $shopping_service->GetShippingCosts($shipp_request);
                                       // dd($shipp_response);
                                        //echo $shipp_response->ShippingCostSummary->ShippingServiceCost->value.PHP_EOL;
                                        $ebay->shipping_fee = $shipp_response->ShippingCostSummary->ShippingServiceCost->value;
                                    }
                                    $ebay->save();
                                }else {
                                    echo $item->globalId .PHP_EOL;
                                }
                                //dd("(%s) %s:%.2f\n",$item->galleryURL,$item->viewItemURL, $item->itemId, $item->title, $item->sellingStatus->currentPrice->value);
                            }
                        }
                    }
                }
            }
            $products2 = Products::where('supplier_id', 2)->get();
            foreach($products2 as $product){
                $product->ebay_price=0;
                $is_null =Ebay::where('product_id',$product->id)->where('shipping_fee','!',NULL)->first();

                if( $is_null ){
                    echo $is_null.PHP_EOL;
                    $ebay_price = DB::select('SELECT MIN( price + shipping_fee )as min_price FROM  `ebays` where product_id ="'.$product->id.'"' );
                    echo($ebay_price[0]->min_price);
                    $product->ebay_price = $ebay_price[0]->min_price;
                }
                $product->save();
            }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
