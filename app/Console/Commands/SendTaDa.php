<?php namespace Tada\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use DB;
use Tada\Products;
use Tada\Img;
use Tada\Category;
use Carbon;

class SendTaDa extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'send:tada';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send data to TaDa Center.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
            
            
            
           //add category
            $category = Category::where('supplier_id','=',2)->orderBy('parent_id', 'asc')->get();
            $prestashop = DB::connection('mysql2');
            $index=1;
            foreach($category as $cat){
                $is_name=$prestashop->select('SELECT `name` FROM `ps_category_lang` where `name` = "'.$cat->name.'"');
                if(!$is_name){
                    
                    $parent = Category::where('supplier_id',2)->where('id',$cat->parent_id)->first();
                    if( $parent == null){
                        $prestashop->insert('INSERT INTO ps_category (id_parent,id_shop_default,active,level_depth,date_add,date_upd,position) VALUES'
                            . ' (13,1,1,4,"'.Carbon::now()->toDateTimeString().'","'.Carbon::now()->toDateTimeString().'","'.++$index.'")');
                    }else{
                     //   $ps_parent_id = $prestashop->select('SELECT * FROM `ps_category_lang` where `name` = "'.$parent->name.'" LIMIT 1');
                        //grab id_parent
                        $ps_category_adn_lang =$prestashop->table('_category')->join('_category_lang', '_category_lang.id_category', '=', '_category.id_category') 
                            ->where('_category_lang.name', '=',$parent->name)->first();
                       // dd($ps_category_adn_lang);
                        $index=$prestashop->table('_category')->where('id_parent',$ps_category_adn_lang->id_category)->count();
                        $prestashop->insert('INSERT INTO ps_category (id_parent,id_shop_default,active,level_depth,date_add,date_upd,position) VALUES'
                            . ' ("'.$ps_category_adn_lang->id_category.'",1,1,5,"'.Carbon::now()->toDateTimeString().'","'.Carbon::now()->toDateTimeString().
                                '","'.$index.'")');
                    }
                    
                    $ps_max_id_category = $prestashop->table('_category')->max('id_category');
                    
                    $prestashop->insert('INSERT INTO ps_category_lang (id_category,id_shop,id_lang,name,link_rewrite) VALUES'
                            . ' ("'.$ps_max_id_category.'",1,1,"'.$cat->name.'","'.$cat->permalink.'")');
                  //  $ps_category=$prestashop->select('SELECT * FROM `ps_category_lang` where `name` = "'.$cat->name.'" LIMIT 1');
                    $prestashop->insert('INSERT INTO ps_category_shop (id_category,id_shop,position) VALUES'
                            . ' ("'. $ps_max_id_category.'",1,1)');
                    for($i=1;$i<=3;$i++){
                        $prestashop->insert('INSERT INTO ps_category_group (id_category,id_group) VALUES'
                                .'("'.$ps_max_id_category.'","'.$i.'")');
                    }
                }
            } 
            echo 'Category import finished '.PHP_EOL;
            //add products
            $products = Products::where('supplier_id','=',2)->get();
            
//            $prestashop->table('_product_lang')->truncate();
//            $prestashop->table('_product')->truncate();
//            $prestashop->table('_product_supplier')->truncate();
//            $prestashop->table('_product_shop')->truncate();
//            $prestashop->table('_category_product')->truncate();
//            $prestashop->table('_stock_available')->truncate();
            
            foreach($products as $pro){
                $permalink=str_replace(' ', '-', strtolower ($pro->sku));
                //$is_product=$prestashop->select('SELECT * FROM `ps_product_lang` where `link_rewrite` = "'.$permalink.'"');
                $is_product=$prestashop->table('_product_lang')->where('link_rewrite',$permalink)->first();
                if(!$is_product){
                    $prestashop->insert('INSERT INTO ps_product (id_supplier,id_shop_default,id_tax_rules_group,'
                        . 'on_sale,online_only,ean13,quantity,price,wholesale_price,active,redirect_type,indexed,date_add,date_upd) VALUES (2,1,"None",0,1,"'
                            .substr($pro->sku,0,13).'","'.$pro->quantity.'","'
                        .$pro->msrp_price.'","'.$pro->supplier_price+1.50.'",1,404,1,"'.Carbon::now()->toDateTimeString().'","'.Carbon::now()->toDateTimeString().'")');
                   
                   
                    $last_id_product=$prestashop->table('_product')->max('id_product');
                    $prestashop->insert('INSERT INTO `ps_product_lang`'
                            . ' (id_product,id_shop,id_lang,link_rewrite,name,description,description_short,'
                            . 'meta_description'
                            . ') VALUES ("'.$last_id_product.'",1,1,"'.$permalink.'","'.str_replace('"', '\"',strip_tags($pro->name)).'","'
                            .str_replace('"', '\"',$pro->description).
                            '","'.str_replace('"', '\"',$pro->short_description).'","'.str_replace('"', '\"',$pro->short_description).'" )');
                    
                    $product_category=DB::table('affiliate_category')->join('categories', 'affiliate_category.category_id', '=', 'categories.id') 
                            ->where('affiliate_category.product_id', '=',$pro->id )->get();
                    
                    $prestashop->insert('INSERT INTO ps_category_product (id_category,id_product) VALUES (12,"'.$last_id_product.'")');
                    $prestashop->insert('INSERT INTO ps_category_product (id_category,id_product) VALUES (13,"'.$last_id_product.'")');
                    foreach($product_category as $pc){
                        $ps_category_lang=$prestashop->table('_category_lang')->where('name',str_replace('"', '\"',$pc->name))->first();
                        if($ps_category_lang->id_category){
                            $prestashop->insert('INSERT INTO ps_category_product (id_category,id_product) VALUES ("'.$ps_category_lang->id_category
                                .'","'.$last_id_product.'")');
                        }
                    }
                    
                   // dd($product_category);
                    
                    $prestashop->insert('INSERT INTO ps_product_supplier (id_product,id_supplier,id_currency) VALUES ("'.$last_id_product.'",2,1 )');
                    if($pro->quantity>2){
                        $active=1;
                    }else {
                        $active=0;
                    }
                        
                    $prestashop->insert('INSERT INTO ps_product_shop (id_product,id_shop,id_category_default,'
                        . 'minimal_quantity,price,wholesale_price,active,redirect_type,indexed,date_add,date_upd) VALUES ("'.$last_id_product.
                            '",1,13,1,"'.$pro->msrp_price.'","'.$pro->supplier_price.'","'.$active.'",404,1,"'.
                            Carbon::now()->toDateTimeString().'","'.Carbon::now()->toDateTimeString().'" )');
                    
                    $prestashop->insert('INSERT INTO ps_stock_available (id_product,id_shop,quantity,out_of_stock) VALUES ("'
                            .$last_id_product.'",1,"'.$pro->quantity.'",2 )');
                    
                    //images
                    
                    $image_path = Img::where('product_id',$pro->id)->get();
                    $img_pah='/home/gieroj/www/tada/public';
                    
                        foreach($image_path as $img){

                            $url = env('PS_SHOP_PATH').'/api/images/products/'.$last_id_product;

                            $img2= $img->location.'/'.$img->img;

                            //curl parameters expects image to be binary :
                            $data_img= array('image'=>"@".$img2.";type=image/jpeg");

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, true);
                            //curl_setopt($ch, CURLOPT_PUT, true); //to modify image chang POST on PUT
                            curl_setopt($ch, CURLOPT_USERPWD, env('PS_WS_AUTH_KEY').':');

                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_img);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            //curl exec
                            if(curl_exec($ch) === false)
                            { echo 'Error : '.curl_error($ch).'<br>'; }
                            else { echo 'Image added: '.$img2.PHP_EOL; }
                            curl_close($ch);	
                        }
                    
                    
                   // dd($last_id_product);
                }else { //if product already is in database
                    $presta_product=$prestashop->table('_product')->where('id_product',$is_product->id_product)->first(); 
                    if($presta_product){
                        $presta_product->ean13 = NULL;
                        $presta_product->quantity = $pro->quantity;
                        $presta_product->price = $pro->msrp_price;
                        $presta_product->wholesale_price = $pro->supplier_price+1.50;
                        $presta_product->date_upd = Carbon::now()->toDateTimeString();
                        //dd($presta_product);
                        $prestashop->table('_product')->where('id_product',$is_product->id_product)->update(get_object_vars($presta_product));
                        
                        
                        $presta_product_lang = $prestashop->table('_product_lang')->where('id_product',$is_product->id_product)->first(); 
                        $presta_product_lang->description = $pro->description;
                        $presta_product_lang->name = strip_tags($pro->name);
                        $presta_product_lang->description_short = $pro->short_description;
                        $presta_product_lang->meta_description = substr($pro->short_description,0,159); //mesta description max 160 char
                                
                        $prestashop->table('_product_lang')->where('id_product',$is_product->id_product)->update(get_object_vars($presta_product_lang));
                        
                        if($pro->quantity > 20 && $pro->status == 1 ){
                            $active=1;
                        }else {
                            $active=0;
                        }
                        $presta_product_shop = $prestashop->table('_product_shop')->where('id_product',$is_product->id_product)->first();
                        $presta_product_shop->price = $pro->msrp_price;
                        $presta_product_shop->wholesale_price = $pro->supplier_price+1.50;
                        $presta_product_shop->active = $active;
                        $presta_product_shop->date_upd = Carbon::now()->toDateTimeString();
                        
                        $prestashop->table('_product_shop')->where('id_product',$is_product->id_product)->update(get_object_vars($presta_product_shop));
                        
                        
                        $presta_stock_available = $prestashop->table('_stock_available')->where('id_product',$is_product->id_product)->first();
                        $presta_stock_available->quantity = $pro->quantity;
                        $prestashop->table('_stock_available')->where('id_product',$is_product->id_product)->update(get_object_vars($presta_stock_available));
                    }
                    
                    $image_path = Img::where('product_id',$pro->id)->get();
                    $img_pah='/home/gieroj/www/tada/public';
                    
                        foreach($image_path as $img){

                            $url = env('PS_SHOP_PATH').'/api/images/products/'.$is_product->id_product;

                            $img2= $img->location.'/'.$img->img;

                            //curl parameters expects image to be binary :
                            $data_img= array('image'=>"@".$img2.";type=image/jpg");

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_PUT, true);
                            //curl_setopt($ch, CURLOPT_PUT, true); //to modify image chang POST on PUT
                            curl_setopt($ch, CURLOPT_USERPWD, env('PS_WS_AUTH_KEY').':');

                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_img);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            //curl exec
                            if(curl_exec($ch) === false)
                            { echo 'Error : '.curl_error($ch).'<br>'; }
                            else { echo 'Image added: '.$img2.PHP_EOL; }
                            curl_close($ch);	
                        }
                }
                
                
                
            } 
            //$users=$prestashop->select('SELECT * FROM  `ps_product`');
            //var_dump($users);
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['example', InputArgument::OPTIONAL, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
