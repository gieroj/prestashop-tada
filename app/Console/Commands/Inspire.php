<?php namespace Tada\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Tada\Products;
use Tada\Ebay;
use Tada\EbayException;
use DB;

class Inspire extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'rapier';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Rapier all bugs in database';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
            Products::where('supplier_id', 2)->update(['status' => 1]);
            Products::where('supplier_id', 2)->where('quantity', 0)->update(['status' => 0]);
            Products::where('supplier_id', 2)->where('supplier_price', 0)->update(['status' => 0]);
            Products::where('supplier_id', 2)->whereRaw('supplier_price = msrp_price')->update(['status' => 0]);
            $products = Products::where('supplier_id', 2)->get();
            foreach($products as $product){
                $product->ebay_price=0;
                $is_null =Ebay::where('product_id',$product->id)->first();
                
                if( $is_null ){
                   // echo $is_null.PHP_EOL;
                    $ebay_price = DB::select('SELECT MIN( price + shipping_fee )as min_price FROM  `ebays` where product_id ='.$product->id );
                    echo $product->id;
                    $this->error(round($ebay_price[0]->min_price,2));
                    $product->ebay_price = round($ebay_price[0]->min_price,2);
                }
                $product->save();
//                $ebayex = EbayException::where('product_id',$product->id)->first(); 
//                if(!isset($ebayex)){
//                    $ebayex = new EbayException();
//                    $ebayex->product_id = $product->id;
//                    $ebayex->category = 117015;
//                    $ebayex->save();
//                }
            }
            $this->comment(PHP_EOL.$this->description.PHP_EOL);
		//$this->comment(PHP_EOL.Inspiring::quote().PHP_EOL);
	}

}
