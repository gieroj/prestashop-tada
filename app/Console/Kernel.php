<?php namespace Tada\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'Tada\Console\Commands\Inspire',
                'Tada\Console\Commands\AZ_Importer',
                'Tada\Console\Commands\SendTaDa',
                'Tada\Console\Commands\eBayImporter',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
                $schedule->command('rapier')->hourly();
                $schedule->command('import:azimport')->dailyAt('8:00');
                $schedule->command('import:ebay')->dailyAt('9:00');
                $schedule->command('send:tada')->dailyAt('10:00');
	}

}
