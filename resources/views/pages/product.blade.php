@extends('pages.index')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-1">
                                        Id: <h4>{{$product->id}}</h4>  
                                    </div>
                                    <div class="col-md-6">
                                        Title: <h4>{{$product->title}}</h4>  
                                    </div>
                                    <div class="col-md-5">
                                        Sku:<h4>{{$product->sku}}</h4>
                                    </div>
                                </div>
                            </div>

				<div class="panel-body">
                                    {!! Form::open(['url' => 'products']) !!}
                                    <div class="form-group">
                                        {!! Form::label('title','Title:') !!}
                                        {!! Form::text('title',$product->title,['class' => 'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::submit('Submit',['class' => 'btn btn-primary form-control']) !!}
                                    </div>
                                    {!! Form::close()!!}
                                    
				</div>
                                  @if ($errors->any())
                                  <ul class="alert alert-danger">
                                      @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                                  @endif
			</div>
		</div>
	</div>
</div>
@endsection
