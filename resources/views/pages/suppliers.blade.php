@extends('pages.index')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Suppliers</div>

				<div class="panel-body">
                                    <ul>
                                    @foreach($suppliers as $supplier)
                                    <article>
                                        <h2>{{$supplier->name}}</h2>
                                        <p>{{$supplier->adress}}</p>
                                    </article>
                                    @endforeach
                                    </ul>
				</div>
                                    
			</div>
		</div>
	</div>
</div>
@endsection
