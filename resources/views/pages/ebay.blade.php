@extends('pages.index')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
                            <div class="panel-heading"> 
                                <div class="row">
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='id')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','id','id')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','id','id') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-2 col-lg-1">
                                        <p>Img </p>
                                    </div>
                                    <div class="col-md-2">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='sku')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','Product','sku')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','Product','sku') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='quantity')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','Quantity','quantity')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','Quantity','quantity') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='supplier_price')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','Supplier price','supplier_price')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','Supplier price','supplier_price') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='msrp_price')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','MSRP','msrp_price')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','MSRP','msrp_price') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='my_price')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','My Price','my_price')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','My Price','my_price') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='ebay_price')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','eBay Price','ebay_price')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','eBay Price','ebay_price') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='profit')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','Profit','profit')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','Profit','profit') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='ebay_profit')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','Ebay Profit','ebay_profit')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','Ebay Profit','ebay_profit') !!}
                                        @endif
                                    </div>
                                </div>
                            </div>

				<div class="panel-body">
                                    {!!$zz=0  !!}
                                   
                                    @foreach($ebays as $ebay)
                                    <div class="row">
                                        <div class="col-md-1">
                                            <p>{{$ebay->id}}</p>
                                        </div>
                                        <div class="thumbnail_img">
                                            @if (isset($img[$zz]->img)){
                                            {!! HTML::image(substr($img[$zz]->location,6).'/'.$img[$zz]->img, 'a picture', array('class' => 'img-thumbnail col-md-2 col-lg-1')) !!}
                                            @else
                                            {!! HTML::image('img/no-product.jpg', 'a picture', array('class' => 'img-thumbnail col-md-2 col-lg-1')) !!}
                                            @endif
                                        </div>
                                        <div class="col-md-2">
                                            <a href="{{ url('/ebay/'.$ebay->id).'/edit' }}"><p>{{$ebay->sku}}</p></a>
                                        </div>
                                        <div class="col-md-1">
                                            <p>{{$ebay->quantity}}</p>
                                        </div>
                                        <div class="col-md-1">
                                            <p>{{$ebay->supplier_price}}</p>
                                        </div>
                                        <div class="col-md-1">
                                            <p>{{$ebay->msrp_price}}</p>
                                        </div>
                                        <div class="col-md-1">
                                            <p>{{$ebay->my_price}}</p>
                                        </div>
                                        <div class="col-md-1">
                                            <p>{{$ebay->ebay_price}}</p>
                                        </div>
                                        <div class="col-md-1">
                                            <p>{{ $ebay->msrp_price - $ebay->supplier_price }}</p>
                                        </div>
                                        <div class="col-md-1">
                                            <p>{{ $ebay->ebay_price - $ebay->supplier_price }}</p>
                                        </div>
                                    </div>
                                    {!! $zz++ !!}
                                    @endforeach
                                    {!! $ebays->render() !!}
				</div>
                               
                                    
			</div>
		</div>
	</div>
</div>
@endsection
