@extends('pages.index')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
                            <div class="panel-heading">
                                
                                <div class="row">
                                    @foreach ($imgs as $img)
                                    {!! HTML::image(substr($img->location,6).'/'.$img->img, 'a picture', array('class' => 'img-thumbnail col-xs-1 ')) !!}
                                    @endforeach
                                </div>
                            </div>

				<div class="tabpanel">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#basic" aria-controls="basic" role="tab" data-toggle="tab">Basic</a></li>
                                            <li role="presentation"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">Description</a></li>
                                            <li role="presentation"><a href="#ebay" aria-controls="ebay" role="tab" data-toggle="tab">eBay</a></li>
                                            <li role="presentation"><a href="#amazon" aria-controls="amazon" role="tab" data-toggle="tab">Amazon</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            
                                           
                                            <div role="tabpanel" class="tab-pane" id="ebay">
                                                
                                                <div class="row">
                                                    <p>{{ $ebay[0]->name }}</p>
                                                </div>
                                                 {!! Form::open(['method' => 'PATCH', 'action' => ['EbayController@update',$ebay[0]->product_id]]) !!}
                                                 {!! Form::submit('Submit',['class' => 'btn btn-primary form-control']) !!}
                                            </div>
                                            </a>
                                            <div role="tabpanel" class="tab-pane" id="amazon">
                                                
                                            </div>
                                            
                                        </div>
                                    
                                   
                                    
				</div>
                                  @if ($errors->any())
                                  <ul class="alert alert-danger">
                                      @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                                  @endif
			</div>
		</div>
	</div>
</div>
@endsection
