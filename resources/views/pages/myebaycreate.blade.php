@extends('pages.index')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
                      
			<div class="panel panel-default">
                            {!! Form::open(['method' => 'PUT', 'action' => ['MyEbayController@update',$product->id]]) !!}
                            <div class="panel-heading"> 
                                <div class="row">
                                    @foreach ($imgs as $img)
                                            <a href="{!! substr($img->location,6).'/'.$img->img !!}" target="_blank">
                                            {!! HTML::image(substr($img->location,6).'/'.$img->img, 'a picture', array('class' => 'img-thumbnail col-xs-1 ')) !!}
                                            </a>
                                            {!! Form::checkbox('img',1,true,['class' => 'control-checkbox col-sm-1']) !!}
                                            
                                        
                                    @endforeach
                                </div>
                                <div class="row">
                                    <div class="col-sm-1">
                                        <p>Name:</p>
                                    </div>
                                    <div class="col-sm-7">
                                        {!! Form::text('name',$product->name,['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <p>Supplier Price: {!! $product->supplier_price !!} </p>
                                    </div>
                                    <div class="col-sm-2">
                                        <p>Ebay Price: {!! $product->ebay_price !!} </p>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            <p>My Ebay Price:</p>
                                        </div>
                                        <div class="col-sm-6">
                                            {!! Form::text('my_price',$product->my_ebay_price,['class' => 'form-control'] ) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <p>Quantity: {!! $product->quantity !!} </p>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            <p>My Ebay Quantity:</p>
                                        </div>
                                        <div class="col-sm-6">
                                            {!! Form::text('quantity',$product->quantity,['class' => 'form-control'] ) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                     <div class="col-sm-2">
                                        <p>product Weight: {!! $product->weight !!} </p>
                                    </div>
                                    <div class="col-sm-2">
                                        <p> {!! Form::select('shipment', $shippers) !!} </p>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="col-sm-6">
                                            <p>Shipping Price:</p>
                                        </div>
                                        <div class="col-sm-6">
                                            {!! Form::text('shipping_price',0,['class' => 'form-control'] ) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-10">
                                        <p> Description </p>
                                        {!! Form::textarea('description',$product->description,['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>

				
                                <div class="form-group">
                                    {!! Form::submit('Submit',['class' => 'btn btn-primary form-control']) !!}
                                </div>
                            {!! Form::close()!!}        
			</div>
		</div>
	</div>
</div>
@endsection
