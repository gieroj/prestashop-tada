@extends('pages.index')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-1">
                                        <h5> Id: {{$product->id}}</h5>  
                                    </div>
                                    <div class="col-md-6">
                                        <h5>Title: {{$product->name}}</h5>  
                                    </div>
                                    <div class="col-md-2">
                                        <h5>Sku: {{$product->sku}}</h5>
                                    </div>
                                    <div class="col-md-3">
                                        <h5>Supplier Price: {{$product->supplier_price}}</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    @foreach ($imgs as $img)
                                    <a href="{!! substr($img->location,6).'/'.$img->img !!}" target="_blank">
                                        {!! HTML::image(substr($img->location,6).'/'.$img->img, 'a picture', array('class' => 'img-thumbnail col-xs-1 ')) !!}
                                    </a>
                                    @endforeach
                                </div>
                            </div>

				<div class="tabpanel">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#basic" aria-controls="basic" role="tab" data-toggle="tab">Basic</a></li>
                                            <li role="presentation"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">Description</a></li>
                                            <li role="presentation"><a href="#ebay" aria-controls="ebay" role="tab" data-toggle="tab">eBay</a></li>
                                            <li role="presentation"><a href="#amazon" aria-controls="amazon" role="tab" data-toggle="tab">Amazon</a></li>
                                            <li role="presentation"><a href="{{ url('/myebay/create/'.$product->id) }}"  >Sell on eBay</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            
                                            <div role="tabpanel" class="tab-pane active" id="basic">
                                                {!! Form::open(['method' => 'PATCH', 'action' => ['ProductsController@update',$product->id]]) !!}
                                                    <div class="form-group row">
                                                        {!! Form::label('name','Title:',['class' => 'control-label col-sm-1']) !!}
                                                        <div class="col-sm-7">
                                                            {!! Form::text('name',$product->name,['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        {!! Form::label('permalink','Permalink:',['class' => 'col-sm-1 control-label']) !!}
                                                        <div class="col-sm-7">
                                                            {!! Form::text('permalink',$product->permalink,['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        {!! Form::label('quantity','Quantity:',['class' => 'col-sm-1 control-label']) !!}
                                                        <div class="col-sm-1">
                                                            {!! Form::text('quantity',$product->quantity,['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        {!! Form::label('supplier_price','Supplier Price:',['class' => 'col-sm-1 control-label']) !!}
                                                        <div class="col-sm-1">
                                                            {!! Form::text('supplier_price',$product->supplier_price,['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        {!! Form::label('msrp_price','MSRP Price:',['class' => 'col-sm-1 control-label']) !!}
                                                        <div class="col-sm-1">
                                                            {!! Form::text('msrp_price',$product->msrp_price,['class' => 'form-control']) !!}
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <p>MSRP Profit: {!! $product->msrp_price - $product->supplier_price !!}</p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        {!! Form::label('my_price','My Price:',['class' => 'col-sm-1 control-label']) !!}
                                                        <div class="col-sm-1">
                                                            {!! Form::text('my_price',$product->my_price,['class' => 'form-control']) !!}
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <p>My Profit: @if($product->my_price){!! $product->my_price - $product->supplier_price !!} @endif</p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        {!! Form::label('weight','Weight:',['class' => 'col-sm-1 control-label']) !!}
                                                        <div class="col-sm-1">
                                                            {!! Form::text('weight',$product->weight,['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>
                                                <div class="form-group">
                                                    {!! Form::submit('Submit',['class' => 'btn btn-primary form-control']) !!}
                                                </div>
                                                {!! Form::close()!!}
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="description">
                                                {!! Form::open(['method' => 'PATCH', 'action' => ['ProductsController@update',$product->id]]) !!}
                                                    <div class="form-group row">
                                                        {!! Form::label('short_description','Short description:',['class' => 'col-sm-1 control-label']) !!}
                                                        <div class="col-sm-10">
                                                            {!! Form::textarea('short_description',$product->short_description,['class' => 'form-control' , 'rows'=>'2']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        {!! Form::label('description','Description:',['class' => 'col-sm-1 control-label']) !!}
                                                        <div class="col-sm-10">
                                                            {!! Form::textarea('description',$product->description,['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>
                                                    </br>
                                                    {!! $product->description !!}
                                                <div class="form-group">
                                                    {!! Form::submit('Submit',['class' => 'btn btn-primary form-control']) !!}
                                                </div>
                                                {!! Form::close()!!}
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="ebay">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h5> My eBay Price: {{$product->my_ebay_price}}</h5>  
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h5>Best eBay Price: {{$product->ebay_price}}</h5>  
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    {!! Form::open(['method' => 'PATCH', 'action' => ['ProductsController@exception_update',$exception->product_id]]) !!}
                                                        
                                                        <div class="col-sm-2">
                                                            <p> eBay Additional Name:</p>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            {!! Form::text('additionalname',$exception->additionalname,['class' => 'form-control']) !!}
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <p> eBay Exception Name:</p>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            {!! Form::text('name',$exception->name,['class' => 'form-control']) !!}
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <p> eBay Additional Category:</p>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            {!! Form::text('category',$exception->category,['class' => 'form-control']) !!}
                                                        </div>
                                                    <div class="form-group">
                                                    {!! Form::submit('Submit',['class' => 'btn btn-primary form-control ']) !!}
                                                </div>
                                                {!! Form::close()!!}
                                                </div>
                                                @foreach($ebay as $eb)
                                                <div class="panel panel-default">
                                                    {!! Form::open(['method' => 'PATCH', 'action' => ['ProductsController@ebay_update',$product->id]]) !!}
                                                    <div class="form-group panel-heading row">
                                                        {!! Form::label('name','Name:',['class' => 'control-label col-sm-1']) !!}
                                                        <div class="col-sm-8">
                                                            {!! Form::text('name',$eb->name,['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="form-group row">
                                                            {!! HTML::image($eb->img, 'ebay picture', array('class' => 'img-thumbnail col-xs-1 ')) !!}
                                                            <div class="form-group ">
                                                                {!! Form::label('price','Price:',['class' => 'control-label col-sm-1']) !!}
                                                                <div class="col-sm-1">
                                                                    {!! Form::text('price',$eb->price,['class' => 'form-control']) !!}
                                                                </div>
                                                                {!! Form::label('shipping_fee','Shipping Fee:',['class' => 'control-label col-sm-1']) !!}
                                                                <div class="col-sm-1">
                                                                    {!! Form::text('shipping_fee',$eb->shipping_fee,['class' => 'form-control']) !!}
                                                                </div>
                                                                {!! Form::label('id','ID:',['class' => 'control-label col-sm-1']) !!}
                                                                <div class="col-sm-1">
                                                                    {!! Form::text('id',$eb->id,['class' => 'form-control']) !!}
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-5"><p> |</p></div>
                                                            <div class="form-group ">
                                                                {!! Form::label('url','url:',['class' => 'control-label col-xs-1']) !!}
                                                                <div class="col-xs-9">
                                                                    {!! Form::text('url',$eb->url,['class' => 'form-control']) !!}
                                                                </div>
                                                                <a href={!! $eb->url !!} class="col-sm-9" target="_blank">{!! $eb->url !!}</a>
                                                            </div>
                                                        </div>
                                                        
                                                        {!! Form::submit('Submit',['class' => 'btn btn-primary form-control ']) !!}
                                                    </div>
                                                    {!! Form::close()!!}
                                                </div>
                                                @endforeach
                                            </div>
                                            </a>
                                            <div role="tabpanel" class="tab-pane" id="amazon">
                                                
                                            </div>
                                            
                                        </div>
                                    
                                   
                                    
				</div>
                                  @if ($errors->any())
                                  <ul class="alert alert-danger">
                                      @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                                  @endif
			</div>
		</div>
	</div>
</div>
@endsection
