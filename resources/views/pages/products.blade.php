@extends('pages.index')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
                            <div class="panel-heading"> 
                                <div class="row">
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='id')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','id','id')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','id','id') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-2 col-lg-1">
                                        <p>Img </p>
                                    </div>
                                    <div class="col-md-2">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='sku')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','Product','sku')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','Product','sku') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='quantity')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','Quantity','quantity')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','Quantity','quantity') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='supplier_price')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','Supplier price','supplier_price')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','Supplier price','supplier_price') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='msrp_price')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','MSRP','msrp_price')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','MSRP','msrp_price') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='my_price')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','My Price','my_price')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','My Price','my_price') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='ebay_price')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','eBay Price','ebay_price')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','eBay Price','ebay_price') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='profit')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','Profit','profit')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','Profit','profit') !!}
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        @if (Request::segment(2)=='sortBy' && Request::segment(3)=='ebay_profit')
                                            
                                                {!! link_to_action('ProductsController@dsortBy','Ebay Profit','ebay_profit')!!}
                                            @else
                                                {!! link_to_action('ProductsController@sortBy','Ebay Profit','ebay_profit') !!}
                                        @endif
                                    </div>
                                </div>
                            </div>

				<div class="panel-body">
                                    
                                    @foreach($products as $product)
                                    <div class="row">
                                        <div class="col-md-1">
                                            <p>{{$product->product_id}}</p>
                                        </div>
                                        <div class="thumbnail_img">
                                            {!! HTML::image(substr($product->location,6).'/'.$product->img, 'a picture', array('class' => 'img-thumbnail col-md-2 col-lg-1')) !!}
                                        </div>
                                        <div class="col-md-2">
                                            <a href="{{ url('/products/'.$product->product_id).'/edit' }}"><p>{{$product->sku}}</p></a>
                                        </div>
                                        <div class="col-md-1">
                                            <p>{{$product->quantity}}</p>
                                        </div>
                                        <div class="col-md-1">
                                            <p>{{$product->supplier_price}}</p>
                                        </div>
                                        <div class="col-md-1">
                                            <p>{{$product->msrp_price}}</p>
                                        </div>
                                        <div class="col-md-1">
                                            <p>{{$product->my_price}}</p>
                                        </div>
                                        <div class="col-md-1">
                                            <p>{{$product->ebay_price}}</p>
                                        </div>
                                        <div class="col-md-1">
                                            <p>{{ $product->msrp_price - $product->supplier_price }}</p>
                                        </div>
                                        <div class="col-md-1">
                                            <p>{{ $product->ebay_price - $product->supplier_price }}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                    {!! $products->render() !!}
				</div>
                               
                                    
			</div>
		</div>
	</div>
</div>
@endsection
