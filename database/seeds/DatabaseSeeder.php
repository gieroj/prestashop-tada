<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Tada\Supplier;
use Tada\Vendor;
use Tada\Category;
class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		 $this->call('SuppliersTableSeeder');
                 $this->call('VendorsTableSeeder');
                 $this->call('CategoriesTableSeeder');
	}

}

class SuppliersTableSeeder extends Seeder {

    public function run()
    {
        DB::table('suppliers')->delete();

        Supplier::create(
            ['name' => 'me',
            'email' => 'pawel@tada.center',
            'active' => true]
            );
        Supplier::create(
            ['name' => 'AZ Importer',
            'email' => 'ok@tada.center',
            'monthly_fee' => 25,
            'per_item_fee' => 1.5,
            'active' => true]
            );
        Supplier::create(
            ['name' => 'eBay',
            'email' => 'ebay@tada.center',
            'per_item_fee' => 0.3,
            'active' => true]
            );
    }

}

class VendorsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('vendors')->delete();

        Vendor::create(
            ['name' => 'me',
            'email' => 'pawel@tada.center',
            'active' => true]
            );
        Vendor::create(
            ['name' => 'eBay',
            'email' => 'ebay@tada.center',
            'website' => 'http://www.ebay.com',
            'per_item_fee' => 0.3,
            'active' => true]
            );
    }

}

class CategoriesTableSeeder extends Seeder {
    public function run()
    {
        DB::table('categories')->delete();
//AZ Importer Category
        Category::create(['supplier_id' => '2','name' => 'R/C Boats','parent_id' => 0,'permalink' =>'rc-boats','ebay' =>2564]);
        Category::create(['supplier_id' => '2','name' => 'R/C Cars','parent_id' => 0,'permalink' =>'rc-cars','ebay' =>19167]);
        Category::create(['supplier_id' => '2','name' => 'R/C Helicopters','parent_id' => 0,'permalink' =>'rc-helicopters','ebay' =>34053]);
        Category::create(['supplier_id' => '2','name' => 'R/C Planes','parent_id' => 0,'permalink' =>'rc-planes','ebay' =>34053]);
        Category::create(['supplier_id' => '2','name' => 'R/C Robots','parent_id' => 0,'permalink' =>'rc-robots','ebay' =>171141]);
        Category::create(['supplier_id' => '2','name' => 'R/C Tanks','parent_id' => 0,'permalink' =>'rc-tanks','ebay' =>44026]);
        Category::create(['supplier_id' => '2','name' => 'R/C Trucks','parent_id' => 0,'permalink' =>'rc-trucks','ebay' =>19167]);
        Category::create(['supplier_id' => '2','name' => 'R/C UFOs','parent_id' => 0,'permalink' =>'rc-ufos','ebay' =>34053]);
        Category::create(['supplier_id' => '2','name' => 'Other RC','parent_id' => 0,'permalink' =>'other-rc','ebay' =>117015]);
        Category::create(['supplier_id' => '2','name' => 'Toy Playset','parent_id' => 0,'permalink' =>'toy-playset','ebay' =>220]);
        Category::create(['supplier_id' => '2','name' => 'Beach Toys','parent_id' => 0,'permalink' =>'beach-toys','ebay' =>145986]);
        Category::create(['supplier_id' => '2','name' => 'Water Toys','parent_id' => 0,'permalink' =>'water-toys','ebay' =>145993]);
        Category::create(['supplier_id' => '2','name' => 'Other Toys','parent_id' => 0,'permalink' =>'other-toys','ebay' =>220]);
        Category::create(['supplier_id' => '2','name' => 'Other Gadgets','parent_id' => 0,'permalink' =>'other-gadgets','ebay' =>31388]);
        Category::create(['supplier_id' => '2','name' => 'Massage Items','parent_id' => 0,'permalink' =>'massage-items','ebay' =>36447]);
        Category::create(['supplier_id' => '2','name' => 'Electronic Etc','parent_id' => 0,'permalink' =>'electronic-etc','ebay' =>14948]);
        Category::create(['supplier_id' => '2','name' => 'New Arrivals','parent_id' => 0,'permalink' =>'new-arrivals','ebay' =>31388]);
        Category::create(['supplier_id' => '2','name' => 'Small Boats','parent_id' => 1,'permalink' =>'small-boats','ebay' =>2564]);
        Category::create(['supplier_id' => '2','name' => 'Large Boats','parent_id' => 1,'permalink' =>'large-boats','ebay' =>2564]);
        Category::create(['supplier_id' => '2','name' => 'Buggies','parent_id' => 2,'permalink' =>'buggies','ebay' =>19167]);
        Category::create(['supplier_id' => '2','name' => 'Cars','parent_id' => 2,'permalink' =>'cars','ebay' =>19167]);
        Category::create(['supplier_id' => '2','name' => 'ESC Cars','parent_id' => 2,'permalink' =>'esc-cars','ebay' =>19167]);
        Category::create(['supplier_id' => '2','name' => 'Licensed Cars','parent_id' => 2,'permalink' =>'licensed-cars','ebay' =>19167]);
        Category::create(['supplier_id' => '2','name' => 'Nitro Cars','parent_id' => 2,'permalink' =>'nitro-cars','ebay' =>19167] );
        Category::create(['supplier_id' => '2','name' => 'Mini Cars','parent_id' => 2,'permalink' =>'mini-cars','ebay' =>19167]);
        Category::create(['supplier_id' => '2','name' => 'Drift Cars','parent_id' => 2,'permalink' =>'draft-cars','ebay' =>19167]);
        Category::create(['supplier_id' => '2','name' => 'Stunt Cars','parent_id' => 2,'permalink' =>'stunt-cars','ebay' =>19167]);
        Category::create(['supplier_id' => '2','name' => 'Small Helis','parent_id' => 3,'permalink' =>'small-helis','ebay' =>34053]);
        Category::create(['supplier_id' => '2','name' => 'Mid Helis','parent_id' => 3,'permalink' =>'mid-helis','ebay' =>34053]);
        Category::create(['supplier_id' => '2','name' => 'Large Helis','parent_id' => 3,'permalink' =>'large-helis','ebay' =>34053]);
        Category::create(['supplier_id' => '2','name' => 'Heli w/Camera','parent_id' => 3,'permalink' =>'heli-wcamera','ebay' =>34053]);
        Category::create(['supplier_id' => '2','name' => 'Quad copters','parent_id' => 3,'permalink' =>'quad-copters','ebay' =>34053]);
        Category::create(['supplier_id' => '2','name' => '2CH Plane','parent_id' => 4,'permalink' =>'2ch-plane','ebay' =>34053]);
        Category::create(['supplier_id' => '2','name' => '3CH Plane','parent_id' => 4,'permalink' =>'3ch-plane','ebay' =>34053]);
        Category::create(['supplier_id' => '2','name' => '4CH Plane','parent_id' => 4,'permalink' =>'4ch-plane','ebay' =>34053]);
        Category::create(['supplier_id' => '2','name' => 'Small Tanks','parent_id' => 6,'permalink' =>'small-tanks','ebay' =>44026]);
        Category::create(['supplier_id' => '2','name' => 'Median Tanks','parent_id' => 6,'permalink' =>'median-tanks','ebay' =>44026]);
        Category::create(['supplier_id' => '2','name' => 'Large Tanks','parent_id' => 6,'permalink' =>'large-tanks','ebay' =>44026]);
        Category::create(['supplier_id' => '2','name' => 'Construction Trucks','parent_id' => 7,'permalink' =>'construction-trucks','ebay' =>2562]);
        Category::create(['supplier_id' => '2','name' => 'Small Trucks','parent_id' => 7,'permalink' =>'small-trucks','ebay' =>19167]);
        Category::create(['supplier_id' => '2','name' => 'Large Trucks','parent_id' => 7,'permalink' =>'large-trucks','ebay' =>19167]);
    }

}