<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbayExceptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ebay_exceptions', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('product_id')->unsigned();
                        $table->foreign('product_id')->references('id')->on('products');
                        $table->string('additionalname')->nullable();
                        $table->string('name')->nullable();
                        $table->string('link')->nullable();
                        $table->integer('category')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ebay_exceptions');
	}

}
