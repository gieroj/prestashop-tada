<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyEbayHistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('my_ebay_histories', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('my_ebay_id')->unsigned();
                        $table->foreign('my_ebay_id')->references('id')->on('my_ebays');
                        $table->double('price', 8, 2);
                        $table->integer('quantity');
                        $table->integer('sold');
                        $table->dateTime('start_at');
                        $table->dateTime('stop_at');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('my_ebay_histories');
	}

}
