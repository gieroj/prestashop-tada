<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('buyers', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('vendor_id')->unsigned();
                        $table->foreign('vendor_id')->references('id')->on('vendors');
                        $table->integer('order_id')->unsigned();
                        $table->foreign('order_id')->references('id')->on('orders');
                        $table->integer('shipment_id')->unsigned();
                        $table->foreign('shipment_id')->references('id')->on('shipments');
                        $table->string('name');
                        $table->integer('additional_id')->nullable();
                        $table->string('nick')->nullable();
                        $table->string('email')->nullable();
                        $table->string('cellphone')->nullable();
                        $table->string('website')->nullable();
                        $table->string('adress')->nullable();
                        $table->integer('zipcode')->nullable();
                        $table->string('state')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('buyers');
	}

}
