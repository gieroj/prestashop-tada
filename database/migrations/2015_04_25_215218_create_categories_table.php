<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categories', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('supplier_id')->unsigned();
                        $table->foreign('supplier_id')->references('id')->on('suppliers');
                        $table->string('name');
                        $table->integer('parent_id');
                        $table->string('permalink')->nullable();
                        $table->integer('ebay')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories');
	}

}
