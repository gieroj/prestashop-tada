<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shipments', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('shipper_id')->unsigned();
                        $table->foreign('shipper_id')->references('id')->on('shippers');
                        $table->integer('order_id')->unsigned();
                        $table->foreign('order_id')->references('id')->on('orders');
                        $table->integer('vendor_id')->unsigned();
                        $table->foreign('vendor_id')->references('id')->on('vendors');
                        $table->boolean('paid');
                        $table->integer('tracking_number')->nullable();
                        $table->integer('tracking_link')->nullable();
                        $table->double('shipping_price', 8, 2)->nullable();
                        $table->double('profit', 8, 2)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shipments');
	}

}
