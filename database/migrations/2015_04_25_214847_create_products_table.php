<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
                        $table->increments('id');
                        $table->integer('supplier_id')->unsigned();
                        $table->foreign('supplier_id')->references('id')->on('suppliers');
                        $table->string('name');
                        $table->string('sku')->nullable();
                        $table->integer('quantity')->nullable();
                        $table->text('description');
                        $table->double('supplier_price', 12, 2)->nullable();
                        $table->double('msrp_price', 12, 2)->nullable();
                        $table->double('minimal_price', 12, 2)->nullable();
                        $table->double('my_price', 12, 2)->nullable();
                        $table->double('ebay_price', 12, 2)->nullable();
                        $table->double('my_ebay_price', 12, 2)->nullable();
                        $table->double('amazon_price', 12, 2)->nullable();
                        $table->double('my_amazon_price', 12, 2)->nullable();
                        $table->string('short_description')->nullable();
                        $table->string('permalink')->nullable();
                        $table->double('weight',10,2)->nullable();
                        $table->boolean('status');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
