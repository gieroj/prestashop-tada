<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('suppliers', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->string('name');
                        $table->string('email')->nullable();
                        $table->string('cellphone')->nullable();
                        $table->string('website')->nullable();
                        $table->string('adress')->nullable();
                        $table->integer('zipcode')->nullable();
                        $table->string('state')->nullable();
                        $table->double('per_item_fee', 8, 2)->nullable();
                        $table->double('monthly_fee', 8, 2)->nullable();
                        $table->boolean('active');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('suppliers');
	}

}
