<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyEbaySpecificsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('my_ebay_specifics', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('myebay_id')->unsigned();
                        $table->foreign('myebay_id')->references('id')->on('my_ebays');
                        $table->string('name');
                        $table->string('description');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('my_ebay_specifics');
	}

}
