<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyEbaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('my_ebays', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('product_id')->unsigned();
                        $table->foreign('product_id')->references('id')->on('products');
                        $table->integer('shipper_id')->unsigned();
                        $table->foreign('shipper_id')->references('id')->on('shippers');
                        $table->boolean('auction');
                        $table->boolean('active');
                        $table->string('name');
                        $table->text('description');
                        $table->integer('quantity');
                        $table->integer('sold');
                        $table->integer('eBay_category');
                        $table->double('supplier_price', 12, 2)->nullable();
                        $table->double('shipping_price', 12, 2)->nullable();
                        $table->double('my_price', 12, 2)->nullable();
                        $table->integer('eBay_number');
                        $table->dateTime('start_at');
                        $table->dateTime('stop_at');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('my_ebays');
	}

}
