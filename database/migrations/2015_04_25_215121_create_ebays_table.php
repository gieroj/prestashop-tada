<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ebays', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('product_id')->unsigned();
                        $table->foreign('product_id')->references('id')
                                ->on('products')->onDelete('cascade');
                        $table->string('name');
                        $table->string('img')->nullable();
                        $table->string('url');
                        $table->string('price');
                        $table->double('shipping_fee',10,2)->nullable();
                        $table->timestamp('update_time');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ebays');
	}

}
